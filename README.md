# Todo command for Mattermost

Adds a channel specific ```/todo``` in Mattermost.

# How to use

## Usage

```
/todo command
```

### Add a TODO item

```
/todo something [today|tomorrow|in 'x' (days|months|weeks)|by next (week|month)]
```

### Viewing the TODO list

```
/todo (list|show)
```

### Deleting a TODO entry

```
/todo (delete|remove|check|done) id
```

## Configuring

1. Edit ```srv.py``` and set ```HOSTNAME```, ```PORT``` and ```TOKEN```
1. Configure a custom slash command in Mattermost as describes [here](https://docs.mattermost.com/developer/slash-commands.html#custom-slash-command)
1. Update ```TOKEN``` variable in ```srv.py```
1. Run ```./srv.py``` to start the server

# Todo
- Add some examples
